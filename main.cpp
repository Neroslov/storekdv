#include <QApplication>
#include "authorization.h"
#include "mainexplorer.h"
#include "report1form.h"

#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    DatabaseManager *dbManager = new DatabaseManager();
    Authorization w(dbManager);
//    MainExplorer w(new User("User", true, true));
//    Report1form w(dbManager);
//    w.CreateTabs();
//    w.show();

    return a.exec();
}
