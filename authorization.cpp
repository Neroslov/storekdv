#include "authorization.h"
#include "ui_authorization.h"

Authorization::Authorization(DatabaseManager *db, QWidget *parent) :
    QDialog(parent), dbManager(db),
    ui(new Ui::Authorization)
{
    ui->setupUi(this);
    show();
}

Authorization::~Authorization()
{
    delete ui;
}

void Authorization::on_Ok_clicked()
{
    auto login = ui->login->text();
    auto password = ui->password->text();

    auto user = dbManager->GetUser(login, password);
    if(user){
        this->close();
        new MainExplorer(dbManager, user);
    }
    ui->checkLabel->setText("Валидация не пройдена");
}
