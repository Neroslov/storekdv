#include "mainexplorer.h"
#include "ui_mainexplorer.h"

#include <QMessageBox>

MainExplorer::MainExplorer(DatabaseManager *db, User* newUser, QWidget *parent) :
    QWidget(parent), dbManager(db), user(newUser),
    ui(new Ui::MainExplorer)
{
    ui->setupUi(this);
    QMessageBox::information(this, "Signed in", "Welcome, " + user->userName);
    CreateTabs();
    show();

    connect(ui->pushButton_2, &QPushButton::clicked, this, [=](){ new ReportList(dbManager); });
}

MainExplorer::~MainExplorer()
{
    delete ui;
}

void MainExplorer::CreateTabs()
{
    if(user->accessSales)
        ui->tabsWidget->addTab(dbManager->CreateTableView(dbManager->selectSelling), "Selling");

    if(user->accessSupplies)
        ui->tabsWidget->addTab(dbManager->CreateTableView(dbManager->selectSupplies), "Supplies");

    if(user->accessSales && user->accessSupplies){
        ui->tabsWidget->addTab(dbManager->CreateTableView(dbManager->selectItems), "Items");
        ui->tabsWidget->addTab(dbManager->CreateTableView(dbManager->selectUsers), "Users");
    }
}
