#ifndef USER_H
#define USER_H

#include <QString>

class User
{
public:
    User();
    User(QString login, bool sales, bool supplies);
    QString userName;
    bool accessSales;
    bool accessSupplies;
};

#endif // USER_H
