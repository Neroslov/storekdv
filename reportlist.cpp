#include "reportlist.h"
#include "ui_reportlist.h"

ReportList::ReportList(DatabaseManager *db, QWidget *parent) :
    QWidget(parent), dbManager(db),
    ui(new Ui::ReportList)
{
    ui->setupUi(this);
    this->show();

    connect(ui->report1, &QPushButton::clicked, this, [=](){ new Report1form(dbManager);});
}

ReportList::~ReportList()
{
    delete ui;
}
