#include "databasemanager.h"
#include <QDebug>

DatabaseManager::DatabaseManager()
{
    QFileInfo *dbPath = new QFileInfo("/home/lizqure/Files/database.db");
    if(!dbPath->exists()){
        dbPath = new QFileInfo(QFileDialog::getOpenFileName(nullptr, "Укажите путь к базе данных"));
        qDebug() << "File not found";
    }

    sqlDatabase = QSqlDatabase::addDatabase("QSQLITE");
    sqlDatabase.setDatabaseName(dbPath->filePath());

    if(!sqlDatabase.open()){
        qDebug() << sqlDatabase.lastError().text();
    }
}

DatabaseManager::~DatabaseManager(){sqlDatabase.close();}

bool DatabaseManager::ExecQuery(QString query)
{
    return ExecQuery(new QSqlQuery(query, sqlDatabase));
}

bool DatabaseManager::ExecQuery(QSqlQuery *query)
{
    if(!query->exec()){
        qDebug() << query->lastError().text();
        return false;
    }
    return true;
}

QTableView *DatabaseManager::CreateTableView(QString query)
{
    QSqlQuery sqlQuery(query, sqlDatabase);

    auto model =  new QSqlQueryModel();
    model->setQuery(query, sqlDatabase);

    auto tabWidget = new QTableView();
    tabWidget->setModel(model);
    tabWidget->hideColumn(0);
    tabWidget->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);

    return tabWidget;
}

User* DatabaseManager::GetUser(QString login, QString password)
{
    auto query = new QSqlQuery(sqlDatabase);
    query->prepare("select Users.login, Users.password, Roles.sales, Roles.supplies \
                   from Users \
                   inner join Roles on Roles.id = Users.role \
                   where Users.login = :user and Users.password = :password");
            query->bindValue(":user", login);
            query->bindValue(":password", password);

    if(ExecQuery(query) && query->next() && !query->value(0).isNull()){
        return new User(
                        query->value("login").toString(),
                        query->value("sales").toBool(),
                        query->value("supplies").toBool()
                    );
    }
    return nullptr;
}
