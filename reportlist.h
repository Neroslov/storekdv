#ifndef REPORTLIST_H
#define REPORTLIST_H

#include <QWidget>
#include "report1form.h"
#include "databasemanager.h"

namespace Ui {
class ReportList;
}

class ReportList : public QWidget
{
    Q_OBJECT

public:
    explicit ReportList(DatabaseManager *db, QWidget *parent = nullptr);
    ~ReportList();

private:
    DatabaseManager *dbManager;
    Ui::ReportList *ui;
};

#endif // REPORTLIST_H
