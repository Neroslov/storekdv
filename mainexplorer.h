#ifndef MAINEXPLORER_H
#define MAINEXPLORER_H

#include <QWidget>
#include <QTableView>
#include "databasemanager.h"
#include "user.h"
#include "reportlist.h"

namespace Ui {
class MainExplorer;
}

class MainExplorer : public QWidget
{
    Q_OBJECT

public:
    explicit MainExplorer(DatabaseManager *db, User* newUser, QWidget *parent = nullptr);
    ~MainExplorer();

public slots:
    void CreateTabs();

private:
    DatabaseManager *dbManager;
    User* user;
    Ui::MainExplorer *ui;
};

#endif // MAINEXPLORER_H
