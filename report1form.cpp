#include "report1form.h"
#include "ui_report1form.h"

#include <QDebug>

Report1form::Report1form(DatabaseManager *db, QWidget *parent) :
    QWidget(parent), dbManager(db),
    ui(new Ui::Report1form)
{
    ui->setupUi(this);

    startDate = new QDateTimeEdit(QDateTime(QDate(2018, 8, 1)));
    endDate = new QDateTimeEdit(QDateTime(QDate(2018,12,31), QTime(23, 59, 59)));
    categoryBox = new QComboBox();

    QSqlQuery categoryQuery("select name from CategoryItem", dbManager->sqlDatabase);
    while(categoryQuery.next())
        categoryBox->addItem(categoryQuery.value(0).toString());

    QPushButton *okButton = new QPushButton("Ok");
    QPushButton *closeButton = new QPushButton("Close");
    QHBoxLayout *startLayout, *endLayout, *buttonLayout;
    QVBoxLayout *sortLayout = new QVBoxLayout();

    startLayout = new QHBoxLayout();
    startLayout->addWidget(new QLabel("От: "));
    startLayout->addWidget(startDate);

    endLayout = new QHBoxLayout();
    endLayout->addWidget(new QLabel("До: "));
    endLayout->addWidget(endDate);

    buttonLayout = new QHBoxLayout();
    buttonLayout->addWidget(okButton);
    buttonLayout->addWidget(closeButton);

    sortLayout->addLayout(startLayout);
    sortLayout->addLayout(endLayout);
    sortLayout->addWidget(categoryBox);
    sortLayout->addLayout(buttonLayout);

    QWidget *dateWidget = new QWidget();
    dateWidget->setWindowTitle("Set range date");
    dateWidget->setLayout(sortLayout);

    connect(okButton, SIGNAL(clicked()), this, SLOT(SelectedOk()));
    connect(okButton, SIGNAL(clicked()), dateWidget, SLOT(close()));
    connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));
    connect(closeButton, SIGNAL(clicked()), dateWidget, SLOT(close()));

    dateWidget->show();
}

Report1form::~Report1form(){delete ui;}

void Report1form::SelectedOk()
{
    this->setFixedSize(592, 842);
    this->show();

    auto startDateTime = startDate->dateTime().toString("yyyy-MM-dd HH:mm:ss");
    auto endDateTime = endDate->dateTime().toString("yyyy-MM-dd HH:mm:ss");
    auto category = categoryBox->currentText();

    GenerateTableReport(startDateTime, endDateTime, category);
    GenerateChartReport(startDateTime, endDateTime, category);
    Print();
}

void Report1form::GenerateTableReport(QString startDate, QString endDate, QString category)
{
    auto stringQueryTable = textQueryTable.arg(startDate).arg(endDate).arg(category);

    QSqlQueryModel *model = new QSqlQueryModel();
    model->setQuery(QSqlQuery(stringQueryTable, dbManager->sqlDatabase));

    ui->tableView->setModel(model);
    ui->tableView->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
}

void Report1form::GenerateChartReport(QString startDate, QString endDate, QString category)
{
    auto queryTextChart = textQueryChart.arg(startDate).arg(endDate).arg(category);
    QSqlQuery query(queryTextChart, dbManager->sqlDatabase);
    auto series = new QtCharts::QHorizontalBarSeries();
    while(query.next()){
        auto name = query.value(0).toString();
        auto sum = query.value(1).toInt();

        auto barSet = new QtCharts::QBarSet(name);
        *barSet << sum;

        series->append(barSet);
    }

    auto chart = new QtCharts::QChart();
    chart->addSeries(series);

    auto axisX = new QValueAxis();
    chart->setAxisX(axisX, series);
    axisX->applyNiceNumbers();

    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);

    auto chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    ui->verticalLayout->addWidget(chartView);
}

void Report1form::Print()
{
    QString fileName = QFileDialog::getSaveFileName(nullptr, "Export PDF", QString(), "*.pdf");
    if (QFileInfo(fileName).suffix().isEmpty())
        fileName.append(".pdf");

    QPrinter printer(QPrinter::PrinterResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setPaperSize(QPrinter::A4);
    printer.setOutputFileName(fileName);
}
