#ifndef AUTHORIZATION_H
#define AUTHORIZATION_H

#include <QDialog>
#include "databasemanager.h"
#include "mainexplorer.h"

namespace Ui {
class Authorization;
}

class Authorization : public QDialog
{
    Q_OBJECT

public:
    explicit Authorization(DatabaseManager *db, QWidget *parent = nullptr);
    ~Authorization();
private slots:
    void on_Ok_clicked();

private:
    DatabaseManager *dbManager;
    Ui::Authorization *ui;
};

#endif // AUTHORIZATION_H
