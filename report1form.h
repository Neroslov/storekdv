#ifndef REPORT1FORM_H
#define REPORT1FORM_H

#include "databasemanager.h"
#include <QWidget>
#include <QDateTimeEdit>
#include <QLabel>
#include <QPushButton>
#include <QComboBox>
#include <QLayout>
#include <QSqlQuery>
#include <QtCharts>
#include <QtPrintSupport/QPrinter>

namespace Ui {
class Report1form;
}

class Report1form : public QWidget
{
    Q_OBJECT

public:
    explicit Report1form(DatabaseManager *db, QWidget *parent = nullptr);
    DatabaseManager *dbManager;
    ~Report1form();

public slots:
    void SelectedOk();

private:
    Ui::Report1form *ui;
    void GenerateTableReport(QString, QString, QString);
    void GenerateChartReport(QString, QString, QString);
    void Print();
    QComboBox *categoryBox;
    QDateTimeEdit *startDate, *endDate;
    QString textQueryTable =
            "select Items.name as 'Товар', sum(RealizationSelling.count_item) as 'Сумма', RealizationSelling.date as 'Дата продажи' \
            from RealizationSelling \
            inner join Items on Items.id = RealizationSelling.id_item \
            inner join CategoryItem on CategoryItem.id = Items.category \
            where RealizationSelling.date > '%1' and RealizationSelling.date < '%2' and CategoryItem.name = '%3' \
            group by id_item \
            order by id_item";

    QString textQueryChart =
            "select Users.login, sum(RealizationSelling.count_item) \
            from RealizationSelling \
            inner join Users on Users.id = RealizationSelling.id_user \
            inner join CategoryItem on CategoryItem.id = RealizationSelling.id_item \
            where RealizationSelling.date > '%1' and RealizationSelling.date < '%2' and CategoryItem.name = '%3' \
            group by Users.login";
};

#endif // REPORT1FORM_H
