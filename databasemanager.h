#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QtSql>
#include <user.h>
#include <QTableView>
#include <QHeaderView>
#include <QFileDialog>

class DatabaseManager
{
public:
    explicit DatabaseManager();
    ~DatabaseManager();
    QTableView* CreateTableView(QString query);
    QSqlDatabase sqlDatabase;
    User* GetUser(QString, QString);
private:
    bool ExecQuery(QString);
    bool ExecQuery(QSqlQuery*);

public:
    QString selectSelling =
            "select RealizationSelling.id, Items.name, RealizationSelling.count_item \
            from Items \
            inner join RealizationSelling \
            on RealizationSelling.id_item = Items.id";

    QString selectSupplies =
            "select Supplies.id, Items.name, Supplies.count_items, Users.login \
            from Supplies \
            inner join Items \
            on Supplies.id_item = Items.id \
            inner join Users \
            on Supplies.id_user = Users.id";

    QString selectItems =
            "select Items.id, Items.Name, Items.price, CategoryItem.name \
            from Items \
            inner join CategoryItem \
            on CategoryItem.id = Items.category";

    QString selectUsers =
            "select Users.id, Users.login, Users.password, Roles.name \
            from Users \
            inner join Roles \
            on Roles.id = Users.role";
};

#endif // DATABASEMANAGER_H
